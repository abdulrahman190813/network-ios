//
//  String+Extension.swift
//  ttt
//
//  Created by Abdulrahman Sabry on 19/09/2021.
//

import Foundation

extension String{
    var trimmed: String {
        return self.trimmingCharacters(in: .whitespacesAndNewlines)
    }
}
