//
//  Validator.swift
//  ttt
//
//  Created by Abdulrahman Sabry on 19/09/2021.
//

import Foundation

class Validator {
    static let shared = Validator()
    
    class func isValidEmail(_ email: String) -> Bool {
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,64}"

        let emailPred = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return emailPred.evaluate(with: email)
    }
}
struct URLs {
    
    static let main = "http://127.0.0.1:8000/api/v1/"
    
    /// POST  {email, password}
    static let login = main + "login"
    
    /// POST  {name, email, password, confirmPassword}
    static let register = main + "register"
}
