//
//  api.swift
//  ttt
//
//  Created by Abdulrahman Sabry on 19/09/2021.
//

import UIKit
import Alamofire
import SwiftyJSON

class api: NSObject {
    
    class func login(email: String, password: String, completion: @escaping (_ error: Error?, _ success: Bool)->Void) {
        
        
        let url = URLs.login
        
        let parameters = [
            "email": email,
            "password": password,
        ]
        Alamofire.request(url, method: .post, parameters: parameters, encoding: URLEncoding.default, headers: nil)
            .validate(statusCode: 200..<300)
            .responseJSON { responds in
                switch responds.result
                {
                case .failure(let error):
                    completion(error, false)
                    print(error)
                
                case .success(let value):
                    let json = JSON(value)
                    
                    if let api_token = json["user"]["api_token"].string {
                        print("api_token: \(api_token)")
                        
                        //save api token to userDefaults
                        completion(nil, true)
                    }
                }
                
            }
        
    }
    class func register(name: String, email: String, password: String, completion: @escaping (_ error: Error?, _ success: Bool)->Void) {
        
        
        let url = URLs.register
        
        let parameters = [
            "name": name,
            "email": email,
            "password": password,
        ]
        Alamofire.request(url, method: .post, parameters: parameters, encoding: URLEncoding.default, headers: nil)
            .validate(statusCode: 200..<300)
            .responseJSON { responds in
                switch responds.result
                {
                case .failure(let error):
                    completion(error, false)
                    print(error)
                
                case .success(let value):
                    let json = JSON(value)
                    
                    if let api_token = json["user"]["api_token"].string {
                        print("api_token: \(api_token)")
                        completion(nil, true)
                    }
                }
                
            }
        
    }
}
