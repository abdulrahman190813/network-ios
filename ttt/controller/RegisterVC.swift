//
//  RegisterVC.swift
//  ttt
//
//  Created by Abdulrahman Sabry on 19/09/2021.
//

import UIKit
import Alamofire
import SwiftyJSON

class RegisterVC: UIViewController {

    @IBOutlet weak var nameTextField: UITextField!
    @IBOutlet weak var emailTextField: UITextField!
    @IBOutlet weak var passwordTextField: UITextField!
    @IBOutlet weak var confirmPasswordTextField: UITextField!
    
    override func viewDidLoad() {
     
        super.viewDidLoad()

        
    }
    
    @IBAction func registerPressed(_ sender: UIButton) {
        guard let name = nameTextField.text, !name.isEmpty else {
            // validation for name
            return
            
        }
        guard let email = emailTextField.text, !email.isEmpty  else {return}
        // validation for correct email
        guard Validator.isValidEmail(email) else {return}
        guard let password = passwordTextField.text, !password.isEmpty  else {return}
        guard let confirmPassword = confirmPasswordTextField.text, !confirmPassword.isEmpty  else {
            return}
        guard confirmPassword == password else {
            return}

        api.register(name: name, email: email, password: password) { (error: Error?, success: Bool) in
            if success {
                print("Register succeed")
            }
        }
    }
}
