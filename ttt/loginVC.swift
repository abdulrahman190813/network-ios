//
//  ViewController.swift
//  ttt
//
//  Created by Abdulrahman Sabry on 18/09/2021.
//

import UIKit
import Alamofire
import SwiftyJSON

class loginVC: UIViewController {
    @IBOutlet weak var emailTF: UITextField!
    
    @IBOutlet weak var passwordTF: UITextField!
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
    }

    @IBAction func loginPressedBTN(_ sender: Any) {
        guard  let email = emailTF.text, !email.isEmpty  else { return }
        guard let password = passwordTF.text, !password.isEmpty else { return }
        api.login(email: email, password: password) { (error: Error?, success: Bool) in
            if success {
                //say welcom to user
            }else{
                //say tray again
            }
        }
        
        
    }
}
